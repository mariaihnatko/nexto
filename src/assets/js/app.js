'use strict';
function myFunction() {
    var dots = document.getElementById("dots");
    var moreText = document.getElementById("more");
    var btnText = document.getElementById("myBtn");
    var news = document.getElementsByClassName("news")[0];
    var news__social = document.getElementsByClassName("news__social")[0];
  
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      btnText.innerHTML = "Czytaj całość"; 
      moreText.style.display = "none";
      mobile.style.display = "none";
    } else {
      dots.style.display = "inline";
      moreText.style.display = "flex";
      btnText.style.display = "none";
      news.style.height = "80rem";
     news__social.style.top = "58rem";
    }      
  }

  $(document).ready(function() {
 
    $('.go_to').click( function(){
      var scroll_el = $(this).attr('href');
      if ($(scroll_el).length != 0) {
        $('html, body').animate({ scrollTop: $(scroll_el).offset().top}, 900);
      }
      return false;
    });
    $('#Btn').on('click', function (){
      $('.video .hidden').removeClass('hidden');
      $('#Btn').addClass('hidden');
    })
    $('#FotoBtn').on('click', function (){
      $('.foto__container .hidden').removeClass('hidden');
      $('#FotoBtn').addClass('hidden');
      $('.foto__container').addClass('all-fotos');
    })
  });
